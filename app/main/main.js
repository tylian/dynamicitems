/**
 * Application initialization
 */

Hooks.once("init", () => {   
//  Items.registerSheet("dnd5e", DynamicItems.DynamicItemSheet5e, {makeDefault: false});
  DynamicItems.setup();
});

Hooks.once("setup", () => {
  DynamicItems.readyActions();
});
