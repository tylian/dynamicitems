/**
 * Application initialization
 */

Hooks.once("init", () => {   
//  Items.registerSheet("dnd5e", DynamicItems.DynamicItemSheet5e, {makeDefault: false});

});

Hooks.once("setup", () => {
  // CONFIG.debug.hooks = true;
  DynamicItems.setup();
  DynamicItems.readyActions();
});
